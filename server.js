const fs = require("fs").promises; // For writing the cookies to a file
const express = require("express");
const Sentry = require("@sentry/node");
const { scrape } = require("./scraper");
const { checkCookies, updateCookies } = require("./cookieHandler");
const { sendData } = require("./dataHandling");
const { connectDB } = require("./db");
require("dotenv").config();

const app = express();
const port = process.env.PORT | 3005;

Sentry.init({
  dsn: process.env.SENTRY_DSN,
  integrations: [
    new Sentry.Integrations.Http({ tracing: true }),
    new Sentry.Integrations.Express({ app }),
    ...Sentry.autoDiscoverNodePerformanceMonitoringIntegrations(),
  ],
  tracesSampleRate: 1.0,
});

app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());

let appStatus = "Ready";

// Connect to the database
try {
  connectDB();
  console.log('Successfully connected to Database');
} catch (err) {
  console.error('Error connecting to Database', err);
  process.exit(1);
}

app.use(express.json());

app.get("/api/scrape", async (req, res) => {
  console.log("Scrape Request Received")
  if (appStatus === "Processing") {
    console.log("Scrape Request Rejected. Reason: Scrape is already in progress")
    return res.status(409).send("Scrape is already in progress");
  }

  try {
    console.log("Scrape Request Accepted")
    res.send("Scrape received");
    appStatus = "Processing";
    console.log("Scrape Started")
    await scrape();
    appStatus = "Done";
    console.log("Scrape Completed")
  } catch (error) {
    appStatus = `Failed with error: ${error.message}`;
    res.status(500).send(`Scrape failed with error: ${error.message}`);
    console.log("Scrape Failed")
  }
});

app.post("/api/update-cookies", async (req, res) => {
  console.log("Cookies Update Received")
  const newCookies = req.body;

  try {
    const response = await updateCookies(newCookies);
    res.send(response);
    console.log("Cookies Update Sent")
  } catch (error) {
    res.status(500).send(`Failed to update cookies with error: ${error.message}`);
    console.log("Cookies Update Failed")
  }
});

// New endpoint for checking cookie validity
app.get("/api/cookie-status", async (req, res) => {
  console.log("Cookie Validity Check Received")
  const status = await checkCookies();
  res.send({ status: status });
  console.log("Cookie Validity Check Sent. Cookie Status: " + status)
});

app.get("/api/send-data/:type", async (req, res) => {
  console.log("Data Send Request Received")
  console.log("Data Send Request Type: " + req.params.type)
  try {
    const { type } = req.params; // get the type parameter from the request URL

    // check if the type parameter is valid
    if (type !== "handouts" && type !== "journal") {
      return res.status(400).send(`Invalid type: ${type}`);
    }

    // send the data and get the response
    const response = await sendData(type);

    res.status(200).json(response);
    console.log("Data Send Request Sent")
    console.log("Data Send Request Response: " + response)
  } catch (error) {
    res.status(500).send(`Failed to send data with error: ${error.message}`);
    console.log("Data Send Request Failed")
    console.log("Data Send Request Error: " + error.message)
  }
});

app.get("/api/status", (req, res) => {
  console.log("App Status Check Received")
  res.send({ status: appStatus });
  console.log("App Status Check Sent. App Status: " + appStatus)
});

app.use(Sentry.Handlers.errorHandler());

// Optional fallthrough error handler
app.use(function onError(err, req, res, next) {
  res.statusCode = 500;
  res.end(res.sentry + "\n");
});

process.on("unhandledRejection", (reason, promise) => {
  console.log("Unhandled Rejection at:", promise, "reason:", reason);
  // You can add any other logic or cleanup code here
});


app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
