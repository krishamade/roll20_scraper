const fs = require("fs").promises;
const moment = require("moment");
const path = require("path");
const Roll20Data = require("./models/roll20Data");  // Path to your Roll20Data model
require("dotenv").config();

// Function to create filename with specific format
const createFileName = (type) => {
  const date = moment().format("YYYYMMDD");
  return path.join(__dirname, "data", `${date}-${type}Export.json`);
};

// Function to delete older files with the same type
const deleteOldFiles = async (type) => {
  const files = await fs.readdir(path.join(__dirname, "data"));
  const filteredFiles = files.filter((file) => file.endsWith(`${type}Export.json`));

  for (const file of filteredFiles) {
    await fs.unlink(path.join(__dirname, "data", file));
  }
};

// Function to export data to a JSON file
const exportData = async (data, type) => {
  const filename = createFileName(type);

  const formattedData = data.map((item) => ({
    Name: item.name,
    Bio: item.bio,
  }));

  try {
    await deleteOldFiles(type);
    await fs.writeFile(filename, JSON.stringify(formattedData, null, 2));
    console.log(`Data has been written to file ${filename}`);
  } catch (err) {
    throw err;
  }
};

async function sendData(type) {
  console.log(`sendData: Starting sendData for type: ${type}`);
  try {
    const filePattern = new RegExp(`^\\d{8}-${type.charAt(0).toUpperCase() + type.slice(1)}Export\\.json$`);
    console.log(`sendData: Created file pattern: ${filePattern}`);

    const today = moment().format('YYYYMMDD');
    const fileName = `${today}-${type.charAt(0).toUpperCase() + type.slice(1)}Export.json`;
    console.log(`sendData: Formed file name: ${fileName}`);

    const dataString = await fs.readFile(`./data/${fileName}`, "utf8");
    const data = JSON.parse(dataString);  
    console.log(`sendData: Read and parsed data: ${data}`);

    let addedCount = 0;
    let updatedCount = 0;

    // Get all existing documents
    const existingDocs = await Roll20Data.find({});
    const existingDocsMap = new Map();
    existingDocs.forEach(doc => existingDocsMap.set(doc.Name, doc));

    for (const item of data) {
      const existingDoc = existingDocsMap.get(item.Name);

      if (existingDoc) {
        // Check if Bio has changed
        if (existingDoc.Bio !== item.Bio) {
          console.log(`sendData: Updating document for Name: ${item.Name}`);
          await Roll20Data.updateOne({ Name: item.Name }, item);
          updatedCount++;  
          console.log(`sendData: Document was updated for Name: ${item.Name}. Updated count: ${updatedCount}`);
        }
      } else {
        console.log(`sendData: Inserting document for Name: ${item.Name}`);
        await new Roll20Data(item).save();
        addedCount++;
        console.log(`sendData: Document was inserted for Name: ${item.Name}. Added count: ${addedCount}`);
      }
    }

    console.log(`${addedCount} documents were added.`);
    console.log(`${updatedCount} documents were updated.`);
    return { addedCount, updatedCount };

  } catch(err) {
    console.error('Error during sendData operation:', err);
    throw err;
  }
}





module.exports = {
  exportData,
  sendData,
};
