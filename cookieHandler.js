const puppeteer = require('puppeteer-extra')
const fs = require("fs").promises;

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

const INDEX_URL = "https://app.roll20.net/campaigns/journal/14655648/index"; // put the correct URL here

async function checkCookies() {
  // Create a new browser instance
  const browser = await puppeteer.launch({
    headless: "new",
    args: ["--no-sandbox", "--disable-setuid-sandbox"],
  });
  const page = await browser.newPage();

  // load cookies
  const cookiesString = await fs.readFile("./cookies.json", "utf8");
  const cookies = JSON.parse(cookiesString);
  await page.setCookie(...cookies);

  let retryCount = 0;
  const MAX_RETRIES = 3; // adjust as per your needs
  const RETRY_DELAY = 5000; // 5000 ms = 5 seconds, adjust as per your needs

  while (retryCount < MAX_RETRIES) {
    try {
      console.log("Navigating to index page...");
      await page.goto(INDEX_URL);

      // Check for "Not Authorized" text
      const notAuthorizedElement = await page.$x("/html/body/div[4]/div/div/div/h1");
      if (notAuthorizedElement.length > 0) {
        const notAuthorizedText = await page.evaluate((el) => el.textContent, notAuthorizedElement[0]);
        if (notAuthorizedText.includes("Not Authorized")) {
          console.log("Not Authorized");
          throw new Error("Not Authorized");
        }
      }

      // if we reach this point, it means the page was loaded successfully, so we can break the loop
      await browser.close();
      return "Valid";
    } catch (error) {
      console.log(`Error occurred, retrying... (${retryCount + 1}/${MAX_RETRIES})`);
      await new Promise((resolve) => setTimeout(resolve, RETRY_DELAY));
      retryCount++;
      if (retryCount >= MAX_RETRIES) {
        console.log("Max retries exceeded.");
        await browser.close();
        return "Invalid";
      }
    }
  }
}
const updateCookies = async (newCookies) => {
  try {
    await fs.writeFile("./cookies.json", JSON.stringify(newCookies, null, 2));
    return "Cookies updated successfully";
  } catch (error) {
    throw error;
  }
};

module.exports = {
  checkCookies,
  updateCookies,
};
