# Base Docker Image
FROM node:16-slim

#Set Docker Container Timezone
ENV TZ=America/Detroit

#Set Environment Variables
ARG HAGGLEBOT_PROD_API_KEY=default_value
ARG HAGGLEBOT_URL=default_value
ENV HAGGLEBOT_PROD_API_KEY=${HAGGLEBOT_PROD_API_KEY}
ENV HAGGLEBOT_URL=${HAGGLEBOT_URL}

# Install Puppeteer dependencies
RUN apt-get update && apt-get install -y \
    ca-certificates \
    fonts-liberation \
    libappindicator3-1 \
    libasound2 \
    libatk-bridge2.0-0 \
    libatk1.0-0 \
    libc6 \
    libcairo2 \
    libcups2 \
    libdbus-1-3 \
    libexpat1 \
    libfontconfig1 \
    libgbm1 \
    libgcc1 \
    libglib2.0-0 \
    libgtk-3-0 \
    libnspr4 \
    libnss3 \
    libpango-1.0-0 \
    libpangocairo-1.0-0 \
    libstdc++6 \
    libx11-6 \
    libx11-xcb1 \
    libxcb1 \
    libxcomposite1 \
    libxcursor1 \
    libxdamage1 \
    libxext6 \
    libxfixes3 \
    libxi6 \
    libxrandr2 \
    libxrender1 \
    libxss1 \
    libxtst6 \
    lsb-release \
    wget \
    xdg-utils

#Create App Directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./
RUN npm install
# Install Puppeteer so it's available in the container.
RUN npm install puppeteer

# Bundle app source
COPY . .

# Copy .env file
COPY .env ./

CMD ["node", "server.js"]
EXPOSE 8990
