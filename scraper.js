const puppeteer = require("puppeteer-extra");
const fs = require("fs");
const { exit } = require("process");
const { exportData } = require("./dataHandling");

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
puppeteer.use(StealthPlugin());

let handouts = [];
let characters = [];

const INDEX_URL = "https://app.roll20.net/campaigns/journal/14655648/index";

async function scrapePage(page, xpath, list, isHandout) {
  console.log("Scraping page...");
  console.log(xpath);

  let url;
  let retryCount = 0;
  const MAX_RETRIES = 3; // adjust as per your needs
  const RETRY_DELAY = 5000; // 5000 ms = 5 seconds, adjust as per your needs

  while (retryCount < MAX_RETRIES) {
    try {
      await Promise.all([await page.goto(INDEX_URL), await page.waitForXPath(xpath, { timeout: 60000 })]);
      const links = await page.$x(xpath);
      if (links.length > 0) {
        await Promise.all([links[0].click(), page.waitForNavigation({ waitUntil: "networkidle0" })]);
        url = page.url();
        console.log(url);
      }

      const nameElement = await page.$(".thisname");
      const name = await page.evaluate((el) => el.textContent, nameElement);

      const bioElement = await page.$(isHandout ? ".thisnotes" : ".thisbio");
      const bio = (await bioElement) ? await page.evaluate((el) => el.textContent, bioElement) : "";

      list.push({
        name,
        bio,
      });

      console.log(list);
      break; // break the loop if page scraped successfully
    } catch (error) {
      if (error instanceof puppeteer.errors.TimeoutError) {
        console.error("Element did not appear within the specified timeout.");
        // Handle the timeout situation as you see fit, possibly by skipping this page or retrying.
      } else {
        // Log the error and retry
        console.log(`Error occurred, retrying... (${retryCount + 1}/${MAX_RETRIES})`);
        retryCount++;
        await new Promise((resolve) => setTimeout(resolve, RETRY_DELAY));
        if (retryCount >= MAX_RETRIES) {
          console.log(`Max retries exceeded for xpath ${xpath}. Moving to next.`);
          break; // Break the loop and move to next xpath if max retries exceeded
        }
      }
    }
  }
}

async function scrape() {
  // Create a new browser instance
  const browser = await puppeteer.launch({
    headless: "new",
    args: ["--no-sandbox", "--disable-setuid-sandbox"],
  });
  const page = await browser.newPage();

  // load cookies
  const cookies = JSON.parse(fs.readFileSync("./cookies.json", "utf8"));
  await page.setCookie(...cookies);

  let retryCount = 0;
  const MAX_RETRIES = 3; // adjust as per your needs
  const RETRY_DELAY = 5000; // 5000 ms = 5 seconds, adjust as per your needs

  while (retryCount < MAX_RETRIES) {
    try {
      console.log("Navigating to index page...");
      await page.goto(INDEX_URL);

      // Check for "Not Authorized" text
      const notAuthorizedElement = await page.$x("/html/body/div[4]/div/div/div/h1");
      if (notAuthorizedElement.length > 0) {
        const notAuthorizedText = await page.evaluate((el) => el.textContent, notAuthorizedElement[0]);
        if (notAuthorizedText.includes("Not Authorized")) {
          console.log("Not Authorized");
          throw new Error("Not Authorized");
        }
      }

      // if we reach this point, it means the page was loaded successfully, so we can break the loop
      break;
    } catch (error) {
      console.log(`Error occurred, retrying... (${retryCount + 1}/${MAX_RETRIES})`);
      await new Promise((resolve) => setTimeout(resolve, RETRY_DELAY));
      retryCount++;
      if (retryCount >= MAX_RETRIES) {
        console.log("Max retries exceeded. Exiting...");
        process.exit(1);
      }
    }
  }

  await page.waitForXPath(`//*[@id="indexlisting"]/div[2]/ul/li[1]/a/div/img`);

  console.log("Extracting character XPaths...");
  const characterXpaths = await page.evaluate(() => {
    const ul = document.querySelector("ul.characters");
    const lis = ul.querySelectorAll("li.listing");

    return Array.from(lis).map((li, index) => `//*[@id="indexlisting"]/div[2]/ul/li[${index + 1}]/a`);
  });

  console.log(`Character XPaths: ${characterXpaths}`);

  console.log("Extracting handout XPaths...");
  const handoutXpaths = await page.evaluate(() => {
    const ul = document.querySelector("ul.handouts");
    const lis = ul.querySelectorAll("li.listing");

    return Array.from(lis).map((li, index) => `//*[@id="indexlisting"]/div[3]/ul/li[${index + 1}]/a`);
  });

  console.log(`Handout XPaths: ${handoutXpaths}`);

  // Scrape each character page
  for (let xpath of characterXpaths) {
    console.log(`Scraping character page with XPath: ${xpath}`);
    await scrapePage(page, xpath, characters, false); // isHandout = false
  }

  // Scrape each handout page
  for (let xpath of handoutXpaths) {
    console.log(`Scraping handout page with XPath: ${xpath}`);
    await scrapePage(page, xpath, handouts, true); // isHandout = true
  }

  // Export the data to JSON files
  console.log("Exporting data...");
  await exportData(handouts, "Handouts");
  await exportData(characters, "Journal");
}

module.exports = {
  scrape,
};
